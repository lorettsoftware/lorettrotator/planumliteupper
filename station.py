from datetime import timedelta
from Connection import *
from Scheduler import Scheduler, supportedStationTypes
from os import listdir, remove, stat
from os.path import join, basename, exists
from pprint import pprint
from time import sleep, time
from threading import Thread, Timer
from requests import post
from pyorbital.orbital import Orbital
from typing import List, Union, NamedTuple
from verboselogs import SPAM

import subprocess
import json



config = {
    "stationName": "planumLite_test",
    
    "port": "auto",
    "controller": "PicoArduino - Board CDC",

    "loggingLevel": "info",
    
    "lat": 43.4001, # in degree
    "lon": 39.9665, # in degree
    "alt": 1,       # in meters
    "timeZone": 3,  # Difference with UTC
    
    "horizon": 10,
    "minApogee": 30,
    
    "schedulerTimer": 6, # In hours
    "scheduleLength": 24, # In hours
    "demo": False,
    "dataPath": "/home/planum/lorett/data/",
    "workingPath": "/home/planum/lorett/upper/",
    "logPath": "/home/planum/lorett/upper/log/",
    "logURL": "http://eus.lorett.org/eus/log_post"
}


__version__ = "1.0.12072022"


def isNumber(num : str) -> bool:
    isNum = True
    try:
        float(num)
    except ValueError:
        isNum = False
    return isNum


# IDK 
def parseConfig(conf) -> dict:
    outputConfig = {}
    messages = []
    
    if isinstance(conf, str):
        if exists(conf):
            with open(conf) as file:
                outputConfig = json.load(file)

            if outputConfig.keys() == config.keys():
                messages.append(("info", f"Config loaded from {conf}"))
                    
            else:
                outputConfig = config
                
                messages.append(("warning", f"Incorrect config in {conf}"))    
                messages.append(("info", "Load default config"))

                with open(conf, 'w') as f:
                    f.write(json.dumps(config, indent=1))
        else:
            outputConfig = config
            
            messages.append(("warning", f"Incorrect config in {conf}"))
            messages.append(("info", "Load default config"))
            
            with open(conf, 'w') as f:
                f.write(json.dumps(config, indent=4))
                    
    elif isinstance(conf, dict):
        if conf.keys() == config.keys():
            outputConfig = conf
            messages.append(("info", f"Config loaded from {conf}"))
                    
        else:
            outputConfig = config
                    
            messages.append(("warning", f"Incorrect config in {conf}"))
            messages.append(("info", "Load default config"))
            
    else:
        outputConfig = config
                    
        messages.append(("warning", f"Incorrect config in {conf}"))
        messages.append(("info", "Load default config"))
        
    return outputConfig, messages


class RepeatingTimer(Thread):
    def __init__(self, interval, function, *args, **kwargs) -> None:
         super(RepeatingTimer, self).__init__()
         self.setDaemon(True)
         self.args = args
         self.kwargs = kwargs
         self.function = function
         self.interval = interval
         
    def start(self) -> None:
        self.callback()
        
    def stop(self) -> None:
        self.interval = False
        
    def callback(self):
        if self.interval:
            self.function(*self.args, **self.kwargs)
            Timer(self.interval, self.callback).start()


class Station:
    def __init__(self, conf: dict, messages: list) -> None:

        self.stationName = conf["stationName"]
        self.port = conf["port"]
        self.lat = conf["lat"]
        self.lon = conf["lon"]
        self.alt = conf["alt"]
        self.horizon = conf["horizon"]
        self.minApogee = conf["minApogee"]
        self.timeZone = conf["timeZone"]
        self.logPath = conf["logPath"]
        self.workingPath = conf["workingPath"]
        self.dataPath = conf["dataPath"]
        self.schedulerTimer = conf["schedulerTimer"] * 60 * 60
        self.scheduleLength = conf["scheduleLength"]
        self.demo = conf["demo"]
        self.device = conf['controller']

        self.logger = LorettLogging(self.stationName, self.logPath, level=conf["loggingLevel"])
        
        if len(messages):
            for type, mes in messages:
                if (type == "info"):
                    self.logger.info(mes)
                    
                if (type == "warning"):
                    self.logger.warning(mes)

        schedulerConf = supportedStationTypes['r8s'].copy()
        schedulerConf['horizon'] = self.horizon
        schedulerConf['minApogee'] = self.minApogee

        self.scheduler = Scheduler(stationName=self.stationName,
                                   lat = self.lat,
                                   lon = self.lon,
                                   alt = self.alt,
                                   path = self.workingPath,
                                   stationType="r8s",
                                   timeZone = self.timeZone,
                                   azimuthCorrection=0,
                                   config=schedulerConf)
        
        
        try:
            self.logger.info("Start first Schedule calculation")
            self.schedule = self.scheduler.getSchedule(timeStart=datetime.now(), length=self.scheduleLength)
            self.logger.info(f"{len(self.schedule)} passes detected in the next {self.scheduleLength} hours")
        
        except Exception as e:
            self.logger.critical(f"Unexpected error in Scheduler: {e}")
            exit(-1)
        
        
        try:
            if self.port == "auto":
                for unit in list_ports.comports():
                    if unit.description == self.device:
                        port = unit.device # get port 
                        self.logger.info(f"The {self.device} device is detected automatically on {port}")
                        break
                else:
                    self.logger.critical(f"The {self.device} device could not be detected. Perhaps the controller is not connected")
                          
        
        except Exception as e:
            self.logger.critical(f"Unexpected error while port auto detect: {e}")      
        
        self.connection = Connection(port = port, logger=self.logger)

        if not self.connection.connect():
            exit(-1)

        self.status = 0     # 0 - wait 1 - running

        self.uploadingLogs = False
        self.watching = False


    def waitNext(self, timeBefore = 90, event="Preparation for"):
        nextSat = self.schedule[0]
        timeStart = nextSat.timeTuple[0]
        timeApogee = nextSat.timeTuple[2]

        startAz = nextSat.orb.get_observer_look(timeStart, lon=self.lon, lat=self.lat, alt=self.alt)[0]
        apogee = nextSat.orb.get_observer_look(timeApogee, lon=self.lon, lat=self.lat, alt=self.alt)[1]
        
        timeStart = (timeStart).strftime("%H:%M:%S")

        waitSeconds = int((nextSat.timeTuple[0] - datetime.utcnow()).total_seconds()) - timeBefore
        

        #self.logger.info(f'Next satellite {nextSat.satName} [azimut: {round(startAz, 2)}\tapogee: {round(apogee, 2)}] at {timeStart} UTC')
        while waitSeconds > 720:    
            self.logger.info(f'Next satellite {nextSat.satName} [azimut: {round(startAz, 2)}\tapogee: {round(apogee, 2)}] at {timeStart} UTC')
            sleep(300)
            waitSeconds -= 300
        
        while waitSeconds > 90:
            if event != "None":
                self.logger.info(f'Next satellite {nextSat.satName} [azimut: {round(startAz, 2)}\tapogee: {round(apogee, 2)}] will begin in {waitSeconds} seconds')
            
            sleep(60)
            waitSeconds -= 60

        
        while waitSeconds > 20:
            if event != "None":
                self.logger.info(f'{event} {nextSat.satName} will begin in {waitSeconds} seconds')
            
            sleep(10)
            waitSeconds -= 10

        while waitSeconds > 1:
            if event != "None":
                self.logger.info(f'{event} {nextSat.satName} will begin in {waitSeconds} seconds')
            
            sleep(1)
            waitSeconds -= 1

    def updateSchedule(self):
        if self.status == 0:
            if len(self.schedule):
                # TODO chancge tuple in Pass on namedTuple
                if len(self.schedule) < 10:
                    if (self.schedule[0].timeTuple[0] - datetime.utcnow()) > timedelta(minutes=10):
                        self.schedule = self.scheduler.getSchedule(timeStart=datetime.now(), length=self.scheduleLength, saveSchedule=True)
                        self.logger.info("Update schedule")

    def watch(self):
        while self.watching:
            
            self.status = 1
           
            satTrack = self.scheduler.getSateliteTrack(self.schedule[0])
            timeStart = self.schedule[0].timeTuple[0]

            self.connection.home()

            if datetime.utcnow() - timeStart > timedelta(minutes=3):
                self.logger.info("Switching to standby mode")
                self.connection.navigate(0, 90, fast=True, timeout=True)
            
            if not len(self.schedule):
                self.updateSchedule()

            self.status = 0

            timeStart = timeStart.strftime("%Y.%m.%d %H:%M:%S")
            passLenght = int((self.schedule[0].timeTuple[1] - self.schedule[0].timeTuple[0]).total_seconds())

            self.waitNext(timeBefore=90, event="Preparation for")
            
            self.status = 1

            self.connection.home()

            self.logger.info(f"Go to SDR calibration pozition: az: {satTrack[0].azimuth} el: {90}")
            self.connection.navigate(satTrack[0].azimuth, 90, True, True)
            
            self.waitNext(timeBefore=20, event="Calibrate for")
            
            self.logger.info(f"Start {self.schedule[0].satName} data recive ({passLenght+20} second left)")
            command = ['python3', join(self.workingPath, "SDRReader.py"),
                            '--s', self.schedule[0].satName,
                            '--t', str(passLenght+20),
                            '--p', self.dataPath]
            
            self.logger.debug(' '.join(command))
            
            if not self.demo:
                subprocess.Popen(command)
            
            self.waitNext(timeBefore=10, event="None")

            self.logger.info(f"Go to start pozition: az: {satTrack[0].azimuth} el: {satTrack[0].elevation}")
            self.connection.navigate(satTrack[0].azimuth, satTrack[0].elevation, True, True)
            
            self.logger.info(f"Wait satellite {self.schedule[0].satName} at {timeStart}")
            self.waitNext(timeBefore=0, event="Tracking")
            
            self.logger.info(f"Start tracking {self.schedule[0].satName}")
            for coordinates in satTrack:
                self.connection.navigate(coordinates.azimuth, coordinates.elevation, False)
                sleep(1)

            self.logger.info(f"Finish tracking {self.schedule[0].satName}")

            sleep(5)
            
            self.logger.info(f"Finish {self.schedule[0].satName} data recive")
            
            self.connection.navigate(coordinates.azimuth, 90, True, True)
            
            self.logger.info("Untangling the wire")
            
            self.connection.comeback()
            self.schedule = self.schedule[1:]


    def uploadLog(self, file: str,  ftype: str = "sdr_rssi_log", url: str = "http://eus.lorett.org/eus/log_post") -> bool:
        """
            Potentially unstable. Attempt to interact with existing infrastructure
        """
        try:
            form = {
                "station": self.stationName,
                "ftype": ftype,
            }
            
            #              del file format
            fileout = file.rsplit('.', 1)[0] + "_rec.log"

            with open(file, 'r') as f1:
                with open(fileout, 'w') as f2:
                    startTime, satellite = basename(file).rsplit('_', 1)
                    satellite = satellite.rsplit('.', 1)[0]

                    f2.write(f"#Pass ID: {basename(file).rsplit('.', 1)[0]}\n")
                    f2.write(f"#Satellite: {satellite}\n")
                    f2.write(f"#Configuration: {satellite}\n")
                    f2.write(f"#Scheduler version: {__version__}\n\n")
                    f2.write(f"#Station: {self.stationName} at {self.lon},{self.lat}\n") # WTF why lot lan instead lat lon

                    formatedTime = datetime.strptime(startTime, "%Y%m%d_%H%M%S")

                    f2.write(f"#Start time: {formatedTime.strftime('%Y-%m-%d %H:%M:%S.000000')}\n\n")
                    f2.write(f"#Time\tAz\tEl\tLevel\tSNR\n")

                    orb = Orbital(satellite, join(self.workingPath, "tle", "tle.txt"))

                    for line in f1:
                        # skip #Configuration line
                        if line[0] == '#':
                            continue

                        date, level, SNR = line.rsplit('\t', 2)
                        dateObj = datetime.strptime(date, "%Y-%m-%d %H:%M:%S.%f")

                        az, el = orb.get_observer_look(dateObj, self.lat, self.lon, self.alt)

                        f2.write(f"{date}\t{round(az, 1)}\t{round(el, 1)}\t{level}\t{SNR}")

            
            with open(fileout, "rb") as f:
                
                files = {
                    "file": f
                }
                
                r = post(url, files=files, data=form)
                
                if r.status_code == 200:
                    self.logger.info(f"Upload log success {basename(file)}")
                    remove(file)

                elif r.status_code == 404:
                    self.logger.warning(f"No connection with server")
                    return False
                
                else:
                    self.logger.error(f"Upload log failed {basename(file)} ({r.status_code})")
                    return False
                
            return True
        
        except Exception as e:
            self.logger.error(f"Upload log failed {basename(file)}")
            self.logger.error(e)
            return False


    def chechLogs(self):
        while self.uploadingLogs:
            try:
                files = [i for i in listdir(self.dataPath) if (i.endswith(".log")) and ('_rec' not in i)]
                for file in files:
                    if (datetime.now() - datetime.fromtimestamp(stat(join(self.dataPath, file)).st_mtime)).total_seconds() < 20:
                        if self.uploadLog(join(self.dataPath, file), ftype="sdr_rssi_log"):
                            sleep(0.01)
                        else:
                            sleep(10)
                    else:
                        sleep(5)


            except Exception as e:
                self.logger.error(f"Error in checkLogs thread: {e}")
                sleep(10)

            sleep(0.05)


    def start(self):
        self.watching = True
        #self.threadWatching = Thread(name = 'watchingThread', target = self.watch)
        #self.threadWatching.start()

        self.uploadLogThread = Thread(target=self.chechLogs, daemon=True)
        #self.uploadLogThread.setDaemon(True)
        self.uploadingLogs = True

        self.ScheduleUpdateTimer = RepeatingTimer(self.schedulerTimer, self.updateSchedule)
        self.ScheduleUpdateTimer.name = "ScheduleUpdateTimer"
        
        self.ScheduleUpdateTimer.start()
        self.logger.info("Schedule timer is started")

        #self.uploadLogThread.start()
        self.logger.info("Upload logs thread is started")

        self.watch()


    def stop(self):
        self.watching = False
        self.uploadingLogs = False
        self.ScheduleUpdateTimer.cancel()


if __name__ == '__main__':
    config, messages = parseConfig("config.json")

    level = logging.DEBUG

    if config['loggingLevel'] == "info":
        config['loggingLevel'] = logging.INFO
    
    elif config['loggingLevel'] == "debug":
        config['loggingLevel'] = logging.DEBUG
    
    elif config['loggingLevel'] == "warning":
        config['loggingLevel'] = logging.WARNING
    
    elif config['loggingLevel'] == "spam":
        config['loggingLevel'] = SPAM

    station = Station(config, messages=messages)
    station.start()