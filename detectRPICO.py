import serial
from serial.tools import list_ports

for unit in list_ports.comports():
    if unit.description in ["PicoArduino - Board CDC", "RaspberryPi Pico"]:
        port = unit.device # get port
        print(f"Pico in {port}")
        break
    else:
        print(f"The {unit.description} device could not be detected. Perhaps the controller is not connected")
