from time import sleep
import serial
from serial.tools import list_ports
from serial.serialutil import SerialException
from datetime import datetime
import logging
from verboselogs import VerboseLogger, SPAM
import coloredlogs
from os.path import join, exists
from os import mkdir
from typing import NoReturn


class LorettLogging:
    '''Класс отвечающий за логирование. Логи пишуться в файл, так же выводться в консоль'''
    def __init__(self, name: str, path: str, level: int) -> NoReturn:
        self.mylogs = VerboseLogger(__name__)
        self.mylogs.setLevel(level)

        self.path = path

        if self.path != "":
            if not exists(self.path):
                mkdir(self.path)

        # обработчик записи в лог-файл
        fileName = datetime.now().strftime(f"{name}_%d-%m-%Y_%H-%M-%S") + ".log"
        fileName = join(self.path, fileName)

        self.file = logging.FileHandler(fileName)
        self.fileformat = logging.Formatter(
            "%(asctime)s:%(levelname)s:%(message)s")

        self.file.setLevel(level)
        self.file.setFormatter(self.fileformat)

        # обработчик вывода в консоль лог файла
        self.stream = logging.StreamHandler()
        self.streamformat = logging.Formatter(
            "%(levelname)s:%(module)s:%(message)s")

        self.stream.setLevel(level)
        self.stream.setFormatter(self.streamformat)

        # инициализация обработчиков
        self.mylogs.addHandler(self.file)
        self.mylogs.addHandler(self.stream)

        coloredlogs.install(level=level, logger=self.mylogs,
                            fmt='%(asctime)s [%(levelname)s] - %(message)s')

        self.mylogs.info('Start Logger')

    def spam(self, message) -> NoReturn:
        '''сообщения спама. например обратная связь с контроллером'''
        self.mylogs.spam(message)
    
    def verbose(self, message) -> NoReturn:
        '''сообщения с пояняющей информацией'''
        self.mylogs.verbose(message)

    def notice(self, message) -> NoReturn:
        self.mylogs.notice(message)

    def debug(self, message) -> NoReturn:
        '''сообщения отладочного уровня'''
        self.mylogs.debug(message)


    def info(self, message) -> NoReturn:
        '''сообщения информационного уровня'''
        self.mylogs.info(message)

    def warning(self, message) -> NoReturn:
        '''не критичные ошибки'''
        self.mylogs.warning(message)

    def critical(self, message) -> NoReturn:
        '''мы почти тонем'''
        self.mylogs.critical(message)
        exit(-1) 

    def error(self, message) -> NoReturn:
        '''ребята я сваливаю ща рванет !!!!'''
        self.mylogs.error(message)


class Connection:
    '''Класс отвечает за связь с нижним уровнем'''
    def __init__(self, logger : LorettLogging,
                       port : str,
                       baudrate : int = 57600,
                       timeout : float = 0.5) -> NoReturn:

        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        self.logger = logger
        self.serial = None

        self.logger.info("init serial Connection class")


    def connect(self) -> bool: 
    # Метод отвечает за подключение
        try:
            self.serial = serial.Serial(port = self.port, baudrate = self.baudrate, timeout = self.timeout)
            self.logger.info(f"Successful connection with {self.port} {self.baudrate} b/s")
            return True

        except SerialException:
            self.logger.error(f"Failed connection with {self.port}")
            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.connect(): {e}")
            return False


    def home(self) -> bool:
        # метод отвечает за поиск точки "дома"
        try:
            self.serial.write('$h;\n'.encode('UTF-8'))
            self.logger.debug(f'Send data: $h;\n')
            self.logger.info("Go to home")
            
            res = self.waitOK(30) and self.waitOK(30)
            
            self.logger.info("In home")

            return res

        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.home(): {e}")

            return False


    def comeback(self) -> bool:
        # метод для возвращения антенны назад
        try:
            self.serial.write('$cb;\n'.encode('UTF-8'))
            self.logger.debug(f'Send data: $cb;\n')
            
            return self.waitOK(15)
        
        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.comeback(): {e}")
            
            return False


    def waitOK(self, timeout: int) -> bool:
        try:
            # метод для ожидания ответа от антенны
            mes = ''
            timeStart = datetime.now()

            self.logger.debug("Start wait lowlevel answer")
            while mes != 'OK' and int((datetime.now() - timeStart).total_seconds()) <= timeout:
                mes = self.serial.readline().decode('UTF-8').strip()
                if len(mes):
                    self.logger.spam(mes)

            if mes == 'OK':
                self.logger.debug("recived OK with lowlevel")
                return True

            self.logger.warning(f"Timeout exceeded - {timeout}")
            return False

        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.waitOK(): {e}")
            
            return False


    def navigate(self, azimuth: float, elevation: float, fast: bool, timeout: bool = False) -> bool:
        try:
            if fast:
                mes = f'$nf {azimuth} {elevation};\n'.encode('UTF-8')
            
            else: 
                mes = f'$n {azimuth} {elevation};\n'.encode('UTF-8')
            
            
            self.serial.write(mes)
            self.logger.debug(f'Send data: {mes}')

            if timeout:
                return self.waitOK(15)
            
            else:
                return True
        
        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.navigate(): {e}")
            
            return False


    def navigateRelative(self, azimuth: float, elevation: float, correction: bool) -> bool:
        try:
            if correction:
                mes = f'$nrc {azimuth} {elevation};\n'.encode('UTF-8')

            else:
                mes = f'$nr {azimuth} {elevation};\n'.encode('UTF-8')
                
            self.serial.write(mes)
            self.logger.debug(f'Send data: {mes}')

            if azimuth >= elevation:
                deg = azimuth

            else:
                deg = elevation

            return self.waitOK(deg*18)
        
        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.navigateRel(): {e}")
            
            return False

        
    def saveCorrection(self) -> bool:
        try:
            self.serial.write('$s;\n'.encode('UTF-8'))
            self.logger.debug(f'Send data: $s;\n')

            return self.waitOK(3)

        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.waitOK(): {e}")
            
            return False


    def clearCorrection(self) -> bool:
        try:
            self.serial.write('$c;\n'.encode('UTF-8'))
            self.logger.debug(f'Send data: $c;\n')

            return self.waitOK(3)
        
        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.waitOK(): {e}")
            
            return False


    def disconnect(self, destructor: bool = False) -> bool:
        if isinstance(self.serial, serial.Serial):
            if self.serial.is_open:
                self.logger.info(f"Disconnect with {self.port}")
                self.serial.close()
            else:
                self.logger.warning(f"Attempt to disconnect again with {self.port}")
            return True

        if not destructor:
            self.logger.warning("Trying to disconnect before connecting")

        return False

    
    def __del__(self) -> NoReturn:
        self.disconnect(destructor=True)





if __name__ == '__main__':
    portName = '/dev/ttyACM0'
    con = Connection(port=portName)
    
    con.connect()
    con.home()
    con.navigate(10, 10, True)
    
    
        